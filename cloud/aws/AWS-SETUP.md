# AWS Setup Procedures

## Initial Setup

### Install CLI

-   `brew install awscli`
-   `brew install aws-iam-authenticator`
-   ` brew install eksctl`

### Create ECR (Elastic Container Registry)

#### Core

-   `aws ecr create-repository --repository-name nemaki-core`

#### Solr

-   `aws ecr create-repository --repository-name nemaki-solr`

### Create EKS Cluster (Elastic Kubernetes Service)

-   `eksctl create cluster --name cmis-staging --region ap-southeast-1`

or with fargate:

-   `eksctl create cluster --name cmis-staging --region ap-southeast-1 --fargate`

#### Create EBS Volume (Elastic Block Store)

-   `aws ec2 create-volume --availability-zone ap-southeast-1a --volume-type gp2 --size 20`
    -> Volume ID: `vol-0ad664f3947b3f5d9`

## ECR Commands

### Docker Login to ECR

-   `aws ecr get-login-password --region ap-southeast-1 | docker login --username AWS --password-stdin 709487830550.dkr.ecr.ap-southeast-1.amazonaws.com`

### Push Images to ECR

#### Core

-   `docker tag local/core:latest 709487830550.dkr.ecr.ap-southeast-1.amazonaws.com/nemaki-core:latest`
-   `docker push 709487830550.dkr.ecr.ap-southeast-1.amazonaws.com/nemaki-core:latest`

#### Solr

-   `docker tag local/solr:latest 709487830550.dkr.ecr.ap-southeast-1.amazonaws.com/nemaki-solr:latest`
-   `docker push 709487830550.dkr.ecr.ap-southeast-1.amazonaws.com/nemaki-solr:latest`

## Deploy Application

-   `kubectl apply -f ./k8s`

## Cleanup

### Delete K8S Resources

-   `kubectl delete -f ./k8s`

### Delete K8S Cluster

-   `eksctl delete cluster --name cmis-staging `

### Delete ECR Images

#### List Images

-   `aws ecr list-images --repository-name nemaki-core`
-   `aws ecr list-images --repository-name nemaki-solr`

#### Batch Delete Images

-   `aws ecr batch-delete-image --repository-name nemaki-core --image-ids <image-id>`
-   `aws ecr batch-delete-image --repository-name nemaki-solr --image-ids <image-id>`

#### Delete ECR Repository

-   `aws ecr delete-repository --repository-name nemaki-core --force`
-   `aws ecr delete-repository --repository-name nemaki-solr --force`
