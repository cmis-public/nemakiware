# GCP Setup Procedures

## Initial Setup

### Install CLI

-   `brew install google-cloud-sdk`

source "/usr/local/Caskroom/google-cloud-sdk/latest/google-cloud-sdk/path.zsh.inc" (in your profile to enable shell command completion for gcloud.)
source "/usr/local/Caskroom/google-cloud-sdk/latest/google-cloud-sdk/completion.zsh.inc" (in your profile to add the Google Cloud SDK command line tools to your $PATH)

-   `glcoud init`

### Create GCR (Google Container Registry)

A GCR is automatically created. No need to create it.

### Create GKE Cluster (Google Kubernetes Engine)

-   `gcloud container clusters create cmis-nemaki-staging --num-nodes=3`

or create it on the gitlab.com web interface at "Operations" -> "Kubernetes".
By creating a cluster via gcloud an entry is automatically added to the kubeconfig in your environment, and the current context changes to that cluster.
If you create a cluster via gitlab.com you manually need to do that by:

-   `gcloud container clusters get-credentials cmis-nemaki-staging`

Zone: `asia-southeast1-b`
Machine type: `e2-small`

#### Create GCE Persistent Disk (Google Compute Engine)

-   `gcloud compute disks create --size=50GB --zone=asia-southeast1-b cmis-nemaki-staging-couchdb`
-   `gcloud compute disks create --size=25GB --zone=asia-southeast1-b cmis-nemaki-staging-solr-nemaki-core`
-   `gcloud compute disks create --size=25GB --zone=asia-southeast1-b cmis-nemaki-staging-solr-token-core`
    </p>
    See also here: https://kubernetes.io/docs/concepts/storage/volumes/#gcepersistentdisk
    Should be at least 200GB because of performance issues. See here for more information: https://developers.google.com/compute/docs/disks#performance.

## GCR Commands

### Docker Auth to GCR

-   `gcloud auth configure-docker`

Writes into `/Users/shomeier/.docker/config.json`

### Push Images to GCR

#### Core

-   `docker tag local/core:latest asia.gcr.io/cmis-nemaki/nemaki-core:latest`
-   `docker push asia.gcr.io/cmis-nemaki/nemaki-core:latest`

#### Solr

-   `docker tag local/solr:latest asia.gcr.io/cmis-nemaki/nemaki-solr:latest`
-   `docker push asia.gcr.io/cmis-nemaki/nemaki-solr:latest`

## Deploy Application

-   `kubectl apply -f ./k8s`

To get the external IP of the LoadBalancer run:

-   `kubectl get service`

Should be: http://35.198.233.29:8080/core/browser/canopy?cmisselector=repositoryInfo

## Scale Nodes

-   `gcloud container clusters resize cmis-nemaki-staging --num-nodes 2`

## Cleanup

### Delete K8S Resources

-   `kubectl delete -f ./k8s`

### Delete K8S Cluster

-   `gcloud container clusters delete cmis-nemaki-staging `

Remove from kubeconfig (list your config by `kubectl config view`)

-   `kubectl config delete-cluster <cluster-name>`
-   `kubectl config delete-context <context-name>`
-   `kubectl config unset users.<user-name>`

Or all by unset cmd:

-   `kubectl config unset users.gke_project_zone_name`
-   `kubectl config unset contexts.aws_cluster1-kubernetes`
-   `kubectl config unset clusters.foobar-baz`

### Delete ECR Images

#### List Images

-   `gcloud container images list --repository asia.gcr.io/cmis-nemaki`

#### Batch Delete Images

-   `gcloud container images delete asia.gcr.io/cmis-nemaki/nemaki-core:latest`
-   `gcloud container images delete asia.gcr.io/cmis-nemaki/nemaki-solr:latest`

#### Delete ECR Repository

-   `aws ecr delete-repository --repository-name nemaki-core --force`
-   `aws ecr delete-repository --repository-name nemaki-solr --force`

### Manage Service Accounts

See: https://cloud.google.com/iam/docs/creating-managing-service-account-keys

### Additional Info

Project Number for 'cmis-nemaki': 137354347254

### Access Control for GCR

For pushing the docker images to GCR you need to add the following two roles to your service account:

-   `gcloud projects add-iam-policy-binding cmis-nemaki --member=serviceAccount:gitlab-ci-container-sa@cmis-nemaki.iam.gserviceaccount.com --role=roles/containerregistry.ServiceAgent`
-   `gcloud projects add-iam-policy-binding cmis-nemaki --member=serviceAccount:gitlab-ci-container-sa@cmis-nemaki.iam.gserviceaccount.com --role=roles/cloudbuild.builds.builder`
-   `gcloud projects add-iam-policy-binding cmis-nemaki --member=serviceAccount:gitlab-ci-container-sa@cmis-nemaki.iam.gserviceaccount.com --role=roles/cloudbuild.builds.builder`

See also here: https://cloud.google.com/container-registry/docs/access-control

### Access Control for GKS

-   `gcloud projects add-iam-policy-binding cmis-nemaki --member=serviceAccount:gitlab-ci-container-sa@cmis-nemaki.iam.gserviceaccount.com --role=roles/container.developer`

See also here for detailed list of kubernetes roles: https://cloud.google.com/iam/docs/understanding-roles#kubernetes-engine-roles

### Change MachineType of a Node Instance

In GCP all nodes must be of the same type.
So to change the machine type of the nodes the simpliest would be to create a new node pool with new machine type and delete the old one.
GCP Web UI can be used here.
