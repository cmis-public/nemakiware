#!/bin/sh

SCRIPT_HOME=$(cd $(dirname $0);pwd)
UTIL_HOME=$SCRIPT_HOME/bjornloka

# Setup single node config: https://docs.couchdb.org/en/stable/setup/single-node.html
curl -X PUT http://127.0.0.1:5984/_users
curl -X PUT http://127.0.0.1:5984/_replicator
curl -X PUT http://127.0.0.1:5984/_global_changes

mvn -f $UTIL_HOME package
java -cp $UTIL_HOME/target/bjornloka.jar jp.aegif.nemaki.bjornloka.Setup $1 $2 $3 $4 $5
mvn -f $UTIL_HOME clean