#!/usr/bin/env bash

# build the cmdline
CMD="java -Dcore.home=\"$CORE_HOME\""
if [ "${DEBUG}" = "true" ]; then
  echo "Enabling debug mode, JPDA accesible under port 8000"
  CMD="$CMD -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=8000"
fi

CMD="$CMD -jar $CORE_HOME/core.jar $SPRING_OPTS $X_OPTS"

if [ -n "${WAIT_FOR}" ]; then
  CMD="wait-for-it.sh --timeout=${WAIT_FOR_TIMEOUT} ${WAIT_FOR} -- ${CMD}"
fi

# shellcheck disable=SC2086
echo "CMD: $CMD"
exec ${CMD}