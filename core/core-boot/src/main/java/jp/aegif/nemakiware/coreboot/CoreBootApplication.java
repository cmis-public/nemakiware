package jp.aegif.nemakiware.coreboot;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.apache.chemistry.opencmis.server.impl.atompub.CmisAtomPubServlet;
import org.apache.chemistry.opencmis.server.impl.browser.CmisBrowserBindingServlet;
import org.glassfish.jersey.servlet.ServletContainer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ImportResource;
import jp.aegif.nemaki.rest.AuthenticationFilter;

@SpringBootApplication(exclude = {
        org.springframework.boot.autoconfigure.gson.GsonAutoConfiguration.class,
        ErrorMvcAutoConfiguration.class // https://stackoverflow.com/questions/34753147/spring-boot-security-does-not-throw-401-unauthorized-exception-but-404-not-found
})
@ImportResource({
        "classpath:applicationContext-boot.xml",
        "classpath*:${context.log}",
        "classpath:propertyContext.xml",
        "classpath*:${context.dao.implementation}",
        "classpath*:${context.dao}",
        "classpath*:${context.businesslogic}",
        "classpath:serviceContext.xml",
        "classpath*:patchContext.xml",
        "classpath*:solrQueryContext.xml"})
// see: https://stackoverflow.com/questions/64479729/legacy-spring-mvc-to-spring-boot
public class CoreBootApplication extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(CoreBootApplication.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(CoreBootApplication.class, args);
    }

    @Bean
    public ServletRegistrationBean<CmisAtomPubServlet> atomPubServletRegistration() {

        Map<String, String> initParams = new HashMap<>();
        initParams.put("callContextHandler", "jp.aegif.nemaki.cmis.factory.auth.NemakiAuthCallContextHandler");
        initParams.put("cmisVersion", "1.1");

        ServletRegistrationBean<CmisAtomPubServlet> registration =
                new ServletRegistrationBean<>(new CmisAtomPubServlet(), "/core/atom/*");
        registration.setLoadOnStartup(1);
        registration.setName("cmisatom");
        registration.setInitParameters(initParams);
        return registration;
    }

    @Bean
    public ServletRegistrationBean<CmisBrowserBindingServlet> browserServletRegistration() {

        ServletRegistrationBean<CmisBrowserBindingServlet> registration =
                new ServletRegistrationBean<>(new CmisBrowserBindingServlet(), "/core/browser/*");
        registration.setLoadOnStartup(1);
        registration.setName("cmisbrowser");
        registration.setInitParameters(
                Collections.singletonMap("callContextHandler",
                        "jp.aegif.nemaki.cmis.factory.auth.NemakiAuthCallContextHandler"));
        return registration;
    }

    @Bean
    public ServletRegistrationBean<ServletContainer> restApiRegistration() {

        Map<String, String> initParams = new HashMap<>();
        initParams.put("jersey.config.server.provider.packages", "jp.aegif.nemaki.rest");
        initParams.put("jersey.config.server.provider.scanning.recursive", "true");
        initParams.put("jersey.config.server.provider.classnames",
                "org.glassfish.jersey.media.multipart.MultiPartFeature");

        ServletRegistrationBean<ServletContainer> registration =
                new ServletRegistrationBean<>(new ServletContainer(), "/core/rest/*");
        registration.setLoadOnStartup(1);
        registration.setName("jersey-app");
        registration.setInitParameters(initParams);
        return registration;
    }

    @Bean
    public FilterRegistrationBean<AuthenticationFilter> loggingFilter(AuthenticationFilter authenticationFilter) {
        FilterRegistrationBean<AuthenticationFilter> registrationBean = new FilterRegistrationBean<>();

        registrationBean.setFilter(authenticationFilter);
        registrationBean.addUrlPatterns("/rest/*");
        registrationBean.setOrder(2);

        return registrationBean;
    }

}
