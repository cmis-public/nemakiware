package jp.aegif.nemakiware.coreboot;

import org.glassfish.jersey.logging.LoggingFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Component
@Configuration
public class JerseyConfig extends ResourceConfig {
    public JerseyConfig() {
        packages("jp.aegif.nemaki.api.resources");
        register(LoggingFeature.class);
    }
}
