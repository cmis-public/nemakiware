<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>jp.aegif.nemakiware</groupId>
	<artifactId>core-boot</artifactId>
	<version>${nemakiware.version}</version>
	<name>core-boot</name>
	<description>NemakiWare Core Server Distribution as Spring Boot Application</description>

	<parent>
		<groupId>jp.aegif.nemakiware</groupId>
		<artifactId>nemakiware-core</artifactId>
		<version>1.0</version>
	</parent>

	<properties>
		<version.spring>2.6.7</version.spring>
	</properties>

	<dependencyManagement>
		<dependencies>
			<dependency>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-dependencies</artifactId>
				<type>pom</type>
				<version>${version.spring}</version>
				<scope>import</scope>
			</dependency>
		</dependencies>
	</dependencyManagement>

	<dependencies>
		<dependency>
			<groupId>jp.aegif.nemaki.core</groupId>
			<artifactId>core-server</artifactId>
			<version>${nemakiware.version}</version>
			<exclusions>
				<exclusion>
					<groupId>org.glassfish.jersey.ext</groupId>
					<artifactId>jersey-spring3</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-jersey</artifactId>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-devtools</artifactId>
			<scope>runtime</scope>
			<optional>true</optional>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
			<scope>test</scope>
		</dependency>

	</dependencies>

	<profiles>
		<profile>
			<id>docker</id>
			<properties>
				<skipTests>true</skipTests>
				<docker.image.repository>${ci.registry.image}/nemaki-core</docker.image.repository>

				<!-- all properties used in Dockerfile for filtering need to be set here again -->
				<!-- <project.build.finalName>${project.build.finalName}</project.build.finalName> -->
			</properties>
			<build>
				<plugins>
					<plugin>
						<groupId>org.eclipse.jkube</groupId>
						<artifactId>kubernetes-maven-plugin</artifactId>
						<version>${jkube.version}</version>
						<executions>
							<execution>
								<!-- bind to install phase since we need the artifacts -->
								<id>docker-build</id>
								<phase>install</phase>
								<goals>
									<goal>build</goal>
									<goal>push</goal>
								</goals>
							</execution>
						</executions>
						<!-- see here for config doc:
							https://www.eclipse.org/jkube/docs/kubernetes-maven-plugin#jkube:build -->
						<configuration>
							<skip>false</skip>
							<images>
								<image>
									<name>${docker.image.repository}:%l</name>
									<build>
										<contextDir>${project.basedir}/src/main/docker</contextDir>
										<dockerFile>Dockerfile</dockerFile>
										<filter>@</filter>
									</build>
								</image>
								<!-- <image>
									<name>${docker.image.repository}-hotswap:%l</name>
									<build>
										<skip>${skip.docker.hotswap.image}</skip>
										<contextDir>${project.basedir}/src/main/docker</contextDir>
										<dockerFile>Dockerfile-hotswap</dockerFile>
										<filter>@</filter>
									</build>
								</image> -->
							</images>
						</configuration>
					</plugin>
				</plugins>
			</build>
		</profile>
	</profiles>

	<build>
		<resources>
			<resource>
				<directory>${basedir}/src/main/resources</directory>
				<filtering>true</filtering>
				<includes>
					<include>**/application*.yml</include>
					<include>**/application*.yaml</include>
					<include>**/application*.properties</include>
				</includes>
			</resource>
			<resource>
				<directory>${basedir}/src/main/resources</directory>
				<excludes>
					<exclude>**/application*.yml</exclude>
					<exclude>**/application*.yaml</exclude>
					<exclude>**/application*.properties</exclude>
				</excludes>
			</resource>
		</resources>
		<plugins>
			<plugin>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-maven-plugin</artifactId>
				<configuration>
					<mainClass>${start-class}</mainClass>

					<!-- 
						Seems jersey is unable to scan nested jars so we need to unpack the jar where the packages
						for jersey are located (see also: JerseyConfig.java)
						See here: https://github.com/spring-projects/spring-boot/issues/1345
					-->
					<requiresUnpack>
						<dependency>
							<groupId>jp.aegif.nemaki.core</groupId>
							<artifactId>core-server</artifactId>
							<version>${nemakiware.version}</version>
						</dependency>
					</requiresUnpack>
				</configuration>
				<executions>
					<execution>
						<id>repackage</id>
						<goals>
							<goal>repackage</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
		</plugins>
	</build>

</project>
