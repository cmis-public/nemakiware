package jp.aegif.nemaki.cmis.aspect.type.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import org.apache.chemistry.opencmis.commons.definitions.TypeDefinitionContainer;
import org.apache.chemistry.opencmis.commons.enums.BaseTypeId;
import org.easymock.EasyMock;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import jp.aegif.nemaki.businesslogic.TypeService;
import jp.aegif.nemaki.cmis.aspect.type.TypeManager;
import jp.aegif.nemaki.cmis.factory.info.RepositoryInfo;
import jp.aegif.nemaki.cmis.factory.info.RepositoryInfoMap;
import jp.aegif.nemaki.dao.ContentDaoService;
import jp.aegif.nemaki.model.NemakiTypeDefinition;
import jp.aegif.nemaki.util.PropertyManager;
import jp.aegif.nemaki.util.constant.SystemConst;
import jp.aegif.nemaki.util.spring.SpringPropertiesUtil;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(loader = AnnotationConfigContextLoader.class)
public class TypeManagerImplTest {

    private static String TEST_REPOSITORY = "test";

    @Autowired
    private TypeManager typeManager;

    @Configuration
    static class ContextConfiguration {

        @Bean
        public TypeManager typeManager() {
            TypeManagerImpl typeManager = new TypeManagerImpl();
            typeManager.setPropertyManager(mockPropertyManager());
            typeManager.setRepositoryInfoMap(mockRepositoryInfoMap());
            typeManager.setTypeService(mockTypeService());
            typeManager.init();

            return typeManager;
        }
    }

    private static TypeService mockTypeService() {
        TypeService typeService = EasyMock.mock(TypeService.class);
        EasyMock.expect(typeService.getTypeDefinitions(TEST_REPOSITORY)).andReturn(createSubTypes());
        EasyMock.expect(typeService.getPropertyDefinitionCores(TEST_REPOSITORY)).andReturn(Collections.emptyList());
        EasyMock.replay(typeService);
        return typeService;
    }

    private static List<NemakiTypeDefinition> createSubTypes() {
        List<NemakiTypeDefinition> retVal = new ArrayList<>();

        NemakiTypeDefinition typeDef = new NemakiTypeDefinition();
        typeDef.setId("testType");
        typeDef.setBaseId(BaseTypeId.CMIS_DOCUMENT);
        typeDef.setParentId(BaseTypeId.CMIS_DOCUMENT.value());
        typeDef.setType("typeDefinition");
        typeDef.setTypeId("testType");
        retVal.add(typeDef);
        return retVal;
    }

    private static RepositoryInfoMap mockRepositoryInfoMap() {
        RepositoryInfoMap repositoryInfoMap = EasyMock.niceMock(RepositoryInfoMap.class);
        EasyMock.expect(repositoryInfoMap.keys()).andReturn(Collections.singleton(TEST_REPOSITORY)).anyTimes();

        RepositoryInfo repositoryInfo = EasyMock.niceMock(RepositoryInfo.class);
        EasyMock.expect(repositoryInfo.getNameSpace()).andReturn("dummyNamespace").anyTimes();
        EasyMock.expect(repositoryInfoMap.get(TEST_REPOSITORY)).andReturn(repositoryInfo).anyTimes();
        EasyMock.replay(repositoryInfo, repositoryInfoMap);

        return repositoryInfoMap;
    }

    private static PropertyManager mockPropertyManager() {
        PropertyManager propertyManager = new PropertyManager();
        ContentDaoService contentDaoService = EasyMock.niceMock(ContentDaoService.class);
        jp.aegif.nemaki.model.Configuration configuration =
                EasyMock.niceMock(jp.aegif.nemaki.model.Configuration.class);
        EasyMock.expect(configuration.getConfiguration()).andReturn(new HashMap<>()).anyTimes();
        EasyMock.expect(contentDaoService.getConfiguration(SystemConst.NEMAKI_CONF_DB)).andReturn(configuration)
                .anyTimes();
        propertyManager.setContentDaoService(contentDaoService);

        SpringPropertiesUtil propertyConfigurer = new SpringPropertiesUtil();
        propertyConfigurer.setIgnoreUnresolvablePlaceholders(true);
        propertyConfigurer.setLocations(
                new ClassPathResource("spring/nemakiware-basetype.properties"),
                new ClassPathResource("spring/nemakiware-capability.properties"),
                new ClassPathResource("spring/nemakiware-property.properties"));
        ConfigurableListableBeanFactory beanFactory = EasyMock.niceMock(ConfigurableListableBeanFactory.class);
        EasyMock.expect(beanFactory.getBeanDefinitionNames()).andReturn(new String[] {}).anyTimes();
        EasyMock.replay(beanFactory);
        propertyConfigurer.postProcessBeanFactory(beanFactory);
        propertyManager.setPropertyConfigurer(propertyConfigurer);
        EasyMock.replay(configuration, contentDaoService);

        return propertyManager;
    }


    @Test
    public void testGetTypeDescendants() throws Exception {

        List<TypeDefinitionContainer> typesDescendants =
                typeManager.getTypesDescendants(TEST_REPOSITORY, null, BigInteger.TEN, Boolean.FALSE);

        typesDescendants.stream()
                .filter(t -> t.getTypeDefinition().getId().equals(BaseTypeId.CMIS_DOCUMENT.value()))
                .map(t -> t.getChildren().get(0))
                .findFirst()
                .ifPresent(t -> assertEquals(t.getTypeDefinition().getId(), "testType"));

        assertEquals(6, typesDescendants.size());
        List<BaseTypeId> rootTypes = Arrays.asList(BaseTypeId.values());
        typesDescendants
                .forEach(t -> assertTrue(rootTypes.contains(BaseTypeId.fromValue(t.getTypeDefinition().getId()))));
    }
}
