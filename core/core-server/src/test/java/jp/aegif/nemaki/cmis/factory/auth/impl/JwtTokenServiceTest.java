package jp.aegif.nemaki.cmis.factory.auth.impl;

import static org.junit.jupiter.api.Assertions.assertThrows;
import org.easymock.EasyMockExtension;
import org.easymock.TestSubject;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import jp.aegif.nemaki.cmis.factory.auth.AuthenticationException;

@ExtendWith(EasyMockExtension.class)
public class JwtTokenServiceTest {

    @TestSubject
    JwtTokenService jwtTokenService = new JwtTokenService();

    @Test
    public void testInvalidTokenFormat() throws Exception {

        assertThrows(AuthenticationException.class, () -> {
            jwtTokenService.verify("InvalidToken");
        });

    }
}
