package jp.aegif.nemaki.cmis.factory.auth.impl;

import java.net.MalformedURLException;
import java.net.URL;
import java.security.interfaces.RSAPublicKey;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.auth0.jwk.InvalidPublicKeyException;
import com.auth0.jwk.Jwk;
import com.auth0.jwk.SigningKeyNotFoundException;
import com.auth0.jwk.UrlJwkProvider;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.JWTVerifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import jp.aegif.nemaki.cmis.factory.auth.AuthenticationException;
import jp.aegif.nemaki.model.exception.ConfigurationException;

@Component
public class JwtTokenService {

    private static Logger LOG = LoggerFactory.getLogger(JwtTokenService.class);

    @Value("${auth.jwks.provider.url}")
    String jwksProviderUrl;

    @Value("${auth.valid.issuer}")
    String validIssuer;

    private Map<String, JWTVerifier> jwtVerifierMap = new HashMap<>();

    public DecodedJWT verify(String token) throws AuthenticationException {

        try {
            DecodedJWT jwt = JWT.decode(token);
            JWTVerifier verifier = jwtVerifierMap.get(jwt.getKeyId());
            if (verifier == null) {
                verifier = getVerifier(jwt);
                jwtVerifierMap.put(jwt.getKeyId(), verifier);
            }
            DecodedJWT verifiedJwt = verifier.verify(token);
            LOG.debug("Successfully verified JWT for subject: {}", verifiedJwt.getSubject());
            return verifier.verify(token);
        } catch (JWTDecodeException e) {
            throw new AuthenticationException("Decoding of JWT token failed", e);
        } catch (JWTVerificationException e) {
            throw new AuthenticationException("Verification of JWT token failed", e);
        }
    }

    public DecodedJWT decode(String token) {

        return JWT.decode(token);
    }

    private JWTVerifier getVerifier(DecodedJWT jwt) throws AuthenticationException {

        try {
            UrlJwkProvider provider = new UrlJwkProvider(new URL(jwksProviderUrl));
            List<Jwk> allJwks = provider.getAll();
            Jwk matchingJwk = allJwks.stream()
                    .filter(jwk -> jwk.getId().equals(jwt.getKeyId()))
                    .findFirst()
                    .orElseThrow(() -> new AuthenticationException(
                            String.format("No signing key with key ID '%s' found on JWK provider: %s", jwt.getKeyId(),
                                    jwksProviderUrl)));

            try {
                RSAPublicKey publicKey = ((RSAPublicKey) matchingJwk.getPublicKey());
                return JWT.require(Algorithm.RSA256(publicKey, null))
                        .withIssuer(validIssuer)
                        .build();
            } catch (InvalidPublicKeyException e) {
                throw new AuthenticationException(
                        String.format("The public key found on JWK provider '%s' for key ID '%' is not an RSA one",
                                jwksProviderUrl, jwt.getKeyId()),
                        e);
            }
        } catch (MalformedURLException e) {
            throw new ConfigurationException("auth.jwks.provider.url", jwksProviderUrl, e);
        } catch (SigningKeyNotFoundException e) {
            throw new AuthenticationException(
                    String.format("No signing keys found at all on JWK provider: %s", jwksProviderUrl), e);
        }
    }
}
