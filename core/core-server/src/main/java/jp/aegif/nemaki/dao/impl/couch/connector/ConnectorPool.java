package jp.aegif.nemaki.dao.impl.couch.connector;

import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;
import org.ektorp.CouchDbConnector;
import org.ektorp.CouchDbInstance;
import org.ektorp.http.HttpClient;
import org.ektorp.http.StdHttpClient;
import org.ektorp.http.StdHttpClient.Builder;
import org.ektorp.impl.StdCouchDbInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import jp.aegif.nemaki.cmis.factory.info.RepositoryInfoMap;
import jp.aegif.nemaki.util.constant.SystemConst;

public class ConnectorPool {

	private static final Logger LOG = LoggerFactory.getLogger(ConnectorPool.class);

	@Autowired
	private DbInitializer dbInitializer;

	private RepositoryInfoMap repositoryInfoMap;
	private String url;
	private int maxConnections;
	private int connectionTimeout;
	private int socketTimeout;
	private boolean authEnabled;
	private String authUserName;
	private String authPassword;

	private Builder builder;
	private Map<String, CouchDbConnector> pool = new HashMap<String, CouchDbConnector>();

	public void init() {
		LOG.info("Initializing connector pool with couch db at url:" + url);

		// Builder
		try {
			this.builder = new StdHttpClient.Builder().url(url).maxConnections(maxConnections)
					.connectionTimeout(connectionTimeout).socketTimeout(socketTimeout)
					.cleanupIdleConnections(true);
		} catch (MalformedURLException e) {
			LOG.error("CouchDB URL is not well-formed!: " + url, e);
			e.printStackTrace();
		}
		if (authEnabled) {
			builder.username(authUserName).password(authPassword);
		}

		// wait for database
		CouchDbInstance dbInstance = new StdCouchDbInstance(builder.build());
		dbInitializer.waitForDb(dbInstance);

		// Inits CouchDB as a single node DB (if not already done)
		dbInitializer.initCouchDbAsSingleNode(dbInstance);

		// Create connector(all-repository config)
		String confDb = SystemConst.NEMAKI_CONF_DB;
		dbInitializer.initNemakiConfDb(dbInstance, confDb);
		add(confDb);
		// initNemakiConfDb();

		// Create connectors
		for (String key : repositoryInfoMap.keys()) {
			dbInitializer.createRepository(key);
			dbInitializer.createRepositoryArchive(repositoryInfoMap.getArchiveId(key));

			add(key);
			add(repositoryInfoMap.getArchiveId(key));
		}
	}

	public CouchDbConnector get(String repositoryId) {
		CouchDbConnector connector = pool.get(repositoryId);
		if (connector == null) {
			throw new Error(
					"CouchDbConnector for repository:" + repositoryId + " cannot be found!");
		}

		return connector;
	}

	public CouchDbConnector add(String repositoryId) {
		CouchDbConnector connector = pool.get(repositoryId);
		if (connector == null) {
			HttpClient httpClient = builder.build();
			CouchDbInstance dbInstance = new StdCouchDbInstance(httpClient);
			boolean dbExists = dbInstance.checkIfDbExists(repositoryId);

			// createConnector throws DbAccessException if db already exists so we need to
			// make this ugly workaround
			if (!dbExists) {
				// should never happen when using DbInitializer
				connector = dbInstance.createConnector(repositoryId, true);
			} else {
				connector = dbInstance.createConnector(repositoryId, false);
			}
			pool.put(repositoryId, connector);
		}

		return connector;
	}

	public void setRepositoryInfoMap(RepositoryInfoMap repositoryInfoMap) {
		this.repositoryInfoMap = repositoryInfoMap;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUrl() {
		return this.url;
	}

	public void setMaxConnections(int maxConnections) {
		this.maxConnections = maxConnections;
	}

	public void setConnectionTimeout(int connectionTimeout) {
		this.connectionTimeout = connectionTimeout;
	}

	public void setSocketTimeout(int socketTimeout) {
		this.socketTimeout = socketTimeout;
	}

	public void setAuthEnabled(boolean authEnabled) {
		this.authEnabled = authEnabled;
	}

	public void setAuthUserName(String authUserName) {
		this.authUserName = authUserName;
	}

	public void setAuthPassword(String authPassword) {
		this.authPassword = authPassword;
	}

	public void setBuilder(Builder builder) {
		this.builder = builder;
	}

	public void setPool(Map<String, CouchDbConnector> pool) {
		this.pool = pool;
	}
}
