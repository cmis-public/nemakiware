/*******************************************************************************
 * Copyright (c) 2013 aegif.
 *
 * This file is part of NemakiWare.
 *
 * NemakiWare is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NemakiWare is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with NemakiWare.
 * If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 * linzhixing(https://github.com/linzhixing) - initial API and implementation
 ******************************************************************************/
package jp.aegif.nemaki.cmis.factory.auth;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.chemistry.opencmis.commons.exceptions.CmisPermissionDeniedException;
import org.apache.chemistry.opencmis.commons.server.CallContext;
import org.apache.chemistry.opencmis.server.shared.BasicAuthCallContextHandler;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import jp.aegif.nemaki.util.PropertyManager;
import jp.aegif.nemaki.util.constant.CallContextKey;
import jp.aegif.nemaki.util.constant.PropertyKey;
import jp.aegif.nemaki.util.spring.SpringContext;

/**
 * Context handler class to do basic authentication
 */
public class NemakiAuthCallContextHandler extends BasicAuthCallContextHandler {


	private static final long serialVersionUID = -8877261669069241258L;
	private static final Logger LOG = LoggerFactory.getLogger(NemakiAuthCallContextHandler.class);

	private static final String AUTHORIZATION_HEADER = "Authorization";
	private static final String BEARER_HEADER_VALUE_PREFIX = "Bearer ";

	/**
	 * Constructor. Initialize authenticationService here.
	 */
	public NemakiAuthCallContextHandler() {
	}

	/**
	 * Return call context map. Throw exception if denied.
	 *
	 * @throws CmisPermissionDeniedException
	 */
	@Override
	public Map<String, String> getCallContextMap(HttpServletRequest request) {
		// Call superclass to get user and password via basic authentication.
		Map<String, String> ctxMap = super.getCallContextMap(request);
		if (ctxMap == null) {
			ctxMap = new HashMap<String, String>();
		}

		// SSO header
		final ApplicationContext applicationContext = SpringContext.getApplicationContext();
		PropertyManager manager = applicationContext.getBean("propertyManager", PropertyManager.class);
		String proxyHeaderKey = manager.readValue(PropertyKey.EXTERNAL_AUTHENTICATION_PROXY_HEADER);
		if (StringUtils.isNotBlank(proxyHeaderKey)) {
			String proxyHeaderVal = request.getHeader(proxyHeaderKey);
			ctxMap.put(proxyHeaderKey, proxyHeaderVal);
			if (StringUtils.isNotBlank(proxyHeaderVal)) {
				ctxMap.put(CallContext.USERNAME, proxyHeaderVal);
			}
		}

		// Normal auth bearer token
		ctxMap.put(CallContextKey.BEARER_AUTH_TOKEN,
				extractBearerToken(request.getHeader(AUTHORIZATION_HEADER)));

		// Nemaki auth token
		ctxMap.put(CallContextKey.NEMAKI_AUTH_TOKEN, request.getHeader(CallContextKey.NEMAKI_AUTH_TOKEN));
		ctxMap.put(CallContextKey.NEMAKI_AUTH_TOKEN_APP, request.getHeader(CallContextKey.NEMAKI_AUTH_TOKEN_APP));

		return ctxMap;
	}

    private String extractBearerToken(String authHeader) {

        if ((authHeader != null) &&
                (authHeader.trim().toLowerCase(Locale.ENGLISH).startsWith(BEARER_HEADER_VALUE_PREFIX))) {
            return authHeader.substring(BEARER_HEADER_VALUE_PREFIX.length());
		}

		return null;
	}
}
