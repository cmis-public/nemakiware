package jp.aegif.nemaki.businesslogic.rendition.impl;

import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.springframework.stereotype.Component;
import jp.aegif.nemaki.businesslogic.rendition.RenditionManager;

@Component("jodRenditionManager")
public class JodRenditionManagerImpl implements RenditionManager {

	@Override
    public ContentStream convertToPdf(ContentStream contentStream, String documentName) {
        throw new UnsupportedOperationException("Converting to PDF not supprted");
	}

	@Override
	public boolean checkConvertible(String mediatype) {
        // we do not support converting to pdf
        return false;
	}
}
