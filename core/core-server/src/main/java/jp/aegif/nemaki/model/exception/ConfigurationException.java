package jp.aegif.nemaki.model.exception;

public class ConfigurationException extends RuntimeException {

    public ConfigurationException(String message) {
        super(message);
    }

    public ConfigurationException(String message, Throwable cause) {
        super(message, cause);
    }

    public ConfigurationException(String key, String value, Throwable cause) {
        super(String.format("Property '%s' misconfigured: %s", key, value), cause);
    }
}
