package jp.aegif.nemaki.util.spring;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.core.Constants;

public class SpringPropertiesUtil extends PropertyPlaceholderConfigurer {

	private static final Logger LOG = LoggerFactory.getLogger(SpringPropertiesUtil.class);

	private Map<String, String> propertiesMap;

	private static final Constants constants = new Constants(PropertyPlaceholderConfigurer.class);
	// Default as in PropertyPlaceholderConfigurer
	private int springSystemPropertiesMode = SYSTEM_PROPERTIES_MODE_FALLBACK;

	@Override
	public void setSystemPropertiesMode(int systemPropertiesMode) {
		LOG.debug("Setting system properties mode to {}", systemPropertiesMode);
		super.setSystemPropertiesMode(systemPropertiesMode);
		springSystemPropertiesMode = systemPropertiesMode;
	}

	@Override
	public void setSystemPropertiesModeName(String constantName) throws IllegalArgumentException {
		this.setSystemPropertiesMode(constants.asNumber(constantName).intValue());
	}

	@Override
	protected void processProperties(ConfigurableListableBeanFactory beanFactory, Properties props)
			throws BeansException {
		super.processProperties(beanFactory, props);

		propertiesMap = new HashMap<String, String>();
		for (Object key : props.keySet()) {
			String keyStr = key.toString();
			String valueStr = resolvePlaceholder(keyStr, props, springSystemPropertiesMode);
			propertiesMap.put(keyStr, valueStr);
		}
	}

	public String getValue(String key) {
		String value = propertiesMap.get(key);

		if (LOG.isTraceEnabled()) {
			LOG.trace("key=" + key + " has no value");
		}

		return value;
	}

	// TODO error handling
	public String getHeadValue(String key) {
		String val = propertiesMap.get(key).toString();
		String[] _val = val.split(",");
		if (_val.length == 0)
			return null;

		return _val[0].trim();
	}

	public Set<String> getKeys() {
		return propertiesMap.keySet();
	}

	public List<String> getValues(String key) {
		try {
			String val = propertiesMap.get(key).toString();
			String[] _val = val.split(",");
			if (_val.length == 0)
				return null;

			List<String> result = new ArrayList<String>();
			for (String _v : _val) {
				result.add(_v.trim());
			}

			return result;
		} catch (Exception e) {
			LOG.error("key=" + key, e);
			return null;
		}
	}
}
