package jp.aegif.nemaki.cmis.factory.auth;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import org.apache.chemistry.opencmis.commons.data.ExtensionsData;
import org.apache.chemistry.opencmis.commons.data.RepositoryInfo;
import org.apache.chemistry.opencmis.commons.exceptions.CmisBaseException;
import org.apache.chemistry.opencmis.commons.exceptions.CmisRuntimeException;
import org.apache.chemistry.opencmis.commons.server.CallContext;
import org.apache.chemistry.opencmis.commons.server.CmisService;
import org.apache.chemistry.opencmis.server.support.wrapper.CmisServiceWrapperManager;
import org.apache.chemistry.opencmis.server.support.wrapper.ConformanceCmisServiceWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import jp.aegif.nemaki.cmis.api.NemakiCmisService;
import jp.aegif.nemaki.util.constant.CallContextKey;

public class CmisServiceWrapper extends ConformanceCmisServiceWrapper implements NemakiCmisService {

	private static Logger LOG = LoggerFactory.getLogger(CmisServiceWrapper.class);

	/**
	 * Constructor used by {@link CmisServiceWrapperManager}.
	 */
	public CmisServiceWrapper(CmisService service) {
		super(service);
	}

	@Override
	public CallContext getCallContext() {
		return ((NemakiCmisService) getWrappedService()).getCallContext();
	}

	/**
	 * Alternative constructor.
	 */
	public CmisServiceWrapper(CmisService service, BigInteger defaultTypesMaxItems,
			BigInteger defaultTypesDepth, BigInteger defaultMaxItems, BigInteger defaultDepth,
			CallContext callContext) {
		super(service, defaultTypesMaxItems, defaultTypesDepth, defaultMaxItems, defaultDepth);
		setCallContext(callContext);
	}

	@Override
	public List<RepositoryInfo> getRepositoryInfos(ExtensionsData extension) {
		Boolean isSu = (Boolean) getCallContext().get(CallContextKey.IS_SU);
		if (isSu) {
			return super.getRepositoryInfos(extension);
		} else {
			List<RepositoryInfo> list = new ArrayList<RepositoryInfo>();
			list.add(getRepositoryInfo(getCallContext().getRepositoryId(), extension));
			return list;
		}
	}

	/**
	 * Converts the given exception into a CMIS exception.
	 */
	@Override
	protected CmisBaseException createCmisException(Exception e) {

		CmisBaseException cbe;
		if (e == null) {
			// should never happen
			// if it happens its the fault of the framework...

			cbe = new CmisRuntimeException("Unknown exception!");
		} else if (e instanceof CmisBaseException) {
			cbe = (CmisBaseException) e;
		} else {
			// should not happen if the connector works correctly

			cbe = new CmisRuntimeException(e.getMessage(), e);
		}

		// log here, otherwise it might got swallowed
		LOG.error("Error: ", cbe);
		return cbe;
	}
}
