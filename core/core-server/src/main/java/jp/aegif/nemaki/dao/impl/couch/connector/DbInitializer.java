package jp.aegif.nemaki.dao.impl.couch.connector;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;
import org.apache.commons.collections.CollectionUtils;
import org.ektorp.CouchDbConnector;
import org.ektorp.CouchDbInstance;
import org.ektorp.DbAccessException;
import org.ektorp.DocumentNotFoundException;
import org.ektorp.ViewQuery;
import org.ektorp.support.DesignDocument;
import org.ektorp.support.DesignDocument.View;
import org.ektorp.support.StdDesignDocumentFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import jp.aegif.nemaki.bjornloka.Load;
import jp.aegif.nemaki.model.Configuration;
import jp.aegif.nemaki.model.couch.CouchConfiguration;

@Component
public class DbInitializer {

    private static final Logger LOG = LoggerFactory.getLogger(DbInitializer.class);

    @Value("${db.couchdb.url}")
    String url;

    public void waitForDb(CouchDbInstance dbInstance) {
        while (true) {
            try {
                LOG.info("Trying to access db at {} ...", url);
                dbInstance.getAllDatabases();
            } catch (DbAccessException dbae) {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException ie) {
                    LOG.error("Error while waiting for database", ie);
                }
                // continue to wait for db ...
                continue;
            }

            // successfully connected
            LOG.info("... successfully connected to db at {}", url);
            break;
        }
    }

    /**
     * On first startup of CouchDB the DB needs to be setup as "Single Node", which atually only
     * means to create three system databases. See also:
     * https://docs.couchdb.org/en/2.3.1/setup/single-node.html
     */
    public void initCouchDbAsSingleNode(CouchDbInstance dbInstance) {
        Set<String> missingSystemDbs = new HashSet<>();

        LOG.info("Checking if CouchDB is set up as single node ...");
        Stream.of("_users", "_replicator", "_global_changes")
                .filter(db -> !dbInstance.checkIfDbExists(db)).forEach(db -> {
                    dbInstance.createDatabase(db);
                    missingSystemDbs.add(db);
                });

        if (missingSystemDbs.isEmpty()) {
            LOG.info("... db is already setup as single node");
        } else {
            LOG.info("... created following missing system databases: {}", missingSystemDbs);
        }
    }

    public void initNemakiConfDb(CouchDbInstance dbInstance, String dbName) {
        boolean dbExists = dbInstance.checkIfDbExists(dbName);
        CouchDbConnector connector;
        if (!dbExists) {
            connector = dbInstance.createConnector(dbName, true);
        } else {
            connector = dbInstance.createConnector(dbName, false);
        }
        addView(connector, "configuration",
                "function(doc) { if (doc.type == 'configuration')  emit(doc._id, doc) }");
        createConfigurationDocument(connector);
    }

    public void createRepository(String repositoryId) {

        String db = repositoryId;
        String dbDumpPath = getResourceAsFile("/db/init.dump").getAbsolutePath();

        loadDbDump(db, dbDumpPath);
    }

    public void createRepositoryArchive(String repositoryArchiveId) {
        String db = repositoryArchiveId;
        String dbDumpPath = getResourceAsFile("/db/init_archive.dump").getAbsolutePath();

        loadDbDump(db, dbDumpPath);
    }

    private DesignDocument getDesignDoc(CouchDbConnector connector, String designDocumentId) {
        StdDesignDocumentFactory factory = new StdDesignDocumentFactory();

        DesignDocument designDoc;
        try {
            designDoc = factory.getFromDatabase(connector, designDocumentId);
        } catch (DocumentNotFoundException e) {
            designDoc = factory.newDesignDocumentInstance();
            designDoc.setId(designDocumentId);
            connector.create(designDoc);
        }

        return designDoc;
    }

    private void addView(CouchDbConnector connector, String viewName, String map) {
        addView(connector, viewName, map, false);
    }

    private void addView(CouchDbConnector connector, String viewName, String map, boolean force) {
        DesignDocument designDoc = getDesignDoc(connector, "_design/_repo");

        if (force || !designDoc.containsView(viewName)) {
            designDoc.addView(viewName, new View(map));
            connector.update(designDoc);
        }
    }

    private void createConfigurationDocument(CouchDbConnector connector) {
        List<CouchConfiguration> list = connector.queryView(
                new ViewQuery().designDocId("_design/_repo").viewName("configuration"),
                CouchConfiguration.class);
        if (CollectionUtils.isEmpty(list)) {
            Configuration configuration = new Configuration();
            connector.create(new CouchConfiguration(configuration));
        }
    }

    private void loadDbDump(String db, String dbDumpPath) {
        LOG.info("Trying to load db dump for {} ...", db);
        Load.main(new String[] {url, db, dbDumpPath, "false"});
    }

    private File getResourceAsFile(String resourcePath) {
        try {
            InputStream in = DbInitializer.class.getResourceAsStream(resourcePath);
            if (in == null) {
                return null;
            }

            File tempFile = File.createTempFile(String.valueOf(in.hashCode()), ".tmp");
            tempFile.deleteOnExit();

            try (FileOutputStream out = new FileOutputStream(tempFile)) {
                // copy stream
                byte[] buffer = new byte[1024];
                int bytesRead;
                while ((bytesRead = in.read(buffer)) != -1) {
                    out.write(buffer, 0, bytesRead);
                }
            }
            return tempFile;
        } catch (IOException e) {
            LOG.error("Error while creating temporary file for CouchDB dump", e);
            return null;
        }
    }
}
