# Run and Debug

## CouchDB

CouchDB can be started via docker-compose file in root directory.

UI will be available at:
http://localhost:5984/\_utils/#

CouchDB Docker Image documentation:
https://github.com/apache/couchdb-docker

Note that for Single Node Setup additional configuration is necessary:
https://docs.couchdb.org/en/stable/setup/single-node.html

## Core and Solr

For VSCode there are tasks in _.vscode/tasks.json_ for running/debugging Core and Solr

For debugging core/solr first start the corresponding debug task and then luanch the corresponding debug session (see also _.vscode/launch.json_)

### Core

Custom configuration file 'custom-nemakiware.properties' can be found in 'core-dist/conf-nemaki' folder.
Configuration directory can be overriden by system property 'nemaki.configDir', see alse 'core/core-dist/src/main/webapp/WEB-INF/classes/propertyContext.xml'

After starting the core the repository URL will be like:
http://localhost:8080/core/browser/canopy?cmisselector=repositoryInfo

**Note** that you need to include the `?cmisselector=repositoryInfo` for browser binding. Need to look into that!

**Also Note** that it is always necessary to include allowable actions! For CMIS Workbench add the following param:

-   cmis.workbench.object.includeAllowableActions=true
-   cmis.workbench.folder.includeAllowableActions=true
-   cmis.workbench.version.includeAllowableActions=true

For a full list of OpenCMIS Workbench see here:
https://chemistry.apache.org/java/developing/tools/dev-tools-workbench.html

### Solr

Solr is available at:
http://localhost:8983/solr/#/

For more information see README.md in _solr_ module
Force Index via URL:
http://localhost:8983/solr/admin/cores?core=nemaki&action=index&tracking=AUTO&repositoryId=canopy

action can be 'index', 'init' or 'change_password'
tracking can be 'AUTO', 'FULL' or 'DELTA'
-> see NemakiCoreAdminHandler.java

## UI

The UI is no longer contained in this repository. You can find it in either:

-   https://github.com/aegif/NemakiWare
-   https://github.com/shomeier/NemakiWare (my fork)

To run it use `activator run` in ui folder

See also: https://github.com/aegif/NemakiWare/wiki/Development_-Development-in-Eclipse

UI available at: http://localhost:8080/ui/repo/bedroom/login

# Misc

Original Wiki from NemakiWare can be found here:
https://github.com/aegif/NemakiWare/wiki
