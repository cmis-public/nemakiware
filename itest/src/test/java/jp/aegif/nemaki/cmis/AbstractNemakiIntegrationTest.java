package jp.aegif.nemaki.cmis;

import org.slf4j.LoggerFactory;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.Network;
import org.testcontainers.containers.output.Slf4jLogConsumer;
import org.testcontainers.containers.wait.strategy.Wait;
import org.testcontainers.images.ImagePullPolicy;
import org.testcontainers.images.PullPolicy;
import org.testcontainers.utility.DockerImageName;
import jp.aegif.nemaki.cmis.tck.TckSuite;

public abstract class AbstractNemakiIntegrationTest extends TckSuite {

	private static final boolean USE_LOCAL_ENV = Boolean.getBoolean("useLocalEnv");

	private static int SOLR_PORT = 8983;
	private static int COUCHDB_PORT = 5984;
	private static int CORE_PORT = 8080;

	private static final String COUCHDB_ALIAS = "couchdb";
	private static final String CORE_ALIAS = "core";
	private static final String SOLR_ALIAS = "solr";

	private static final String DEFAULT_CORE_DOCKER_IMAGE =
			"registry.gitlab.com/cmis-public/nemakiware/nemaki-core:latest";
	private static final String DEFAULT_SOLR_DOCKER_IMAGE =
			"registry.gitlab.com/cmis-public/nemakiware/nemaki-solr:latest";
	private static final String CORE_DOCKER_IMAGE = System.getProperty("core.docker.image", DEFAULT_CORE_DOCKER_IMAGE);
	private static final String SOLR_DOCKER_IMAGE = System.getProperty("solr.docker.image", DEFAULT_SOLR_DOCKER_IMAGE);

	private static final boolean NO_PULL = Boolean.getBoolean("noPull");
	private static final ImagePullPolicy PULL_POLICY = (NO_PULL) ? PullPolicy.defaultPolicy() : PullPolicy.alwaysPull();

	protected static final String REPOSITORY = "canopy";
	protected static final String USER = "admin";
	protected static final String PASSWORD = "admin";

	private static Network network = Network.newNetwork();

	protected static GenericContainer<?> couchDb;
	protected static GenericContainer<?> core;
	protected static GenericContainer<?> solr;

	protected static String atomPubUrl = "http://localhost:8080/core/atom/canopy";
	protected static String browserUrl = "http://localhost:8080/core/browser/canopy?cmisselector=repositoryInfo";

	static {

		String host = "localhost";
		Integer port = 8080;

		if (!USE_LOCAL_ENV) {

			couchDb = new GenericContainer<>(DockerImageName.parse("couchdb:2.3.1"))
					.withExposedPorts(COUCHDB_PORT)
					.withImagePullPolicy(PULL_POLICY)
					.withNetwork(network)
					.withNetworkAliases(COUCHDB_ALIAS)
					.waitingFor(Wait.forHttp("/"));
			couchDb.start();

			core = new GenericContainer<>(
					DockerImageName.parse(CORE_DOCKER_IMAGE))
							.withExposedPorts(CORE_PORT)
							.withImagePullPolicy(PULL_POLICY)
							.withNetwork(network)
							.withNetworkAliases(CORE_ALIAS)
							.dependsOn(couchDb)
							.withEnv("db.couchdb.url",
									String.format("http://%s:%s", COUCHDB_ALIAS, COUCHDB_PORT))
							.withEnv("solr.host", SOLR_ALIAS)
                            .withLogConsumer(new Slf4jLogConsumer(LoggerFactory.getLogger("core")))
                            .waitingFor(
                                    Wait.forHttp("/core/browser/canopy?cmisselector=repositoryInfo")
                                            .forStatusCode(401));
			core.start();

			solr = new GenericContainer<>(
					DockerImageName.parse(SOLR_DOCKER_IMAGE))
							.withExposedPorts(SOLR_PORT)
							.withImagePullPolicy(PULL_POLICY)
							.withNetwork(network)
							.withNetworkAliases(SOLR_ALIAS)
							.dependsOn(core)
							.withEnv("cmis.server.host", CORE_ALIAS)
							.withEnv("cmis.server.port", String.valueOf(CORE_PORT))
                            .withLogConsumer(new Slf4jLogConsumer(LoggerFactory.getLogger("solr")))
							.waitingFor(
									Wait.forLogMessage(
											".*o.a.s.c.SolrCore \\[\\b(token|nemaki)\\b\\] Registered new searcher Searcher.*",
                                            2));
			solr.start();

			host = core.getHost();
			port = core.getMappedPort(CORE_PORT);
		}

		atomPubUrl = String.format("http://%s:%s/core/atom/%s", host, port, REPOSITORY);
		browserUrl = String.format("http://%s:%s/core/browser/%s?cmisselector=repositoryInfo", host, port, REPOSITORY);
		System.setProperty("org.apache.chemistry.opencmis.binding.atompub.url", atomPubUrl);
		System.setProperty("org.apache.chemistry.opencmis.binding.browser.url", browserUrl);

	}
}
