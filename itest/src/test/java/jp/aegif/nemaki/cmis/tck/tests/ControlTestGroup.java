package jp.aegif.nemaki.cmis.tck.tests;

import org.apache.chemistry.opencmis.tck.tests.control.ACLSmokeTest;
import org.junit.Ignore;
import org.junit.Test;
import jp.aegif.nemaki.cmis.AbstractNemakiIntegrationTest;

public class ControlTestGroup extends AbstractNemakiIntegrationTest {
	@Test
	@Ignore("The corresponding method applyAcl is not implemented on server side in AclService (note that there are two overloaded methods applyAcl)")
	public void aclSmokeTest() throws Exception {
		ACLSmokeTest test = new ACLSmokeTest();
		run(test);
	}
}
