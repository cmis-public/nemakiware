The only files which are important for running inside a Solr Docker container are located in folder './solr'.

The files in './src/main/contexts', './src/main/etc', './src/main/resources' and './src/main/webapp' are only important when running Solr locally with 'mvn -Dsolr.solr.home=./solr/solr-dist/solr -Dsolr.log.dir=./logs jetty:run'
