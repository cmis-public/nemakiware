# Run / Debug

See _.vscode/tasks.json_ in root dir for Run / Debug tasks

# Modules

## solr-dist

Builds and runs a solr distribution.

## solr-plugin

Contains the nemakiware extensions/plugins to index the contents of a CMIS repository by using the content change log. See also API description below.

# API

## Initialization and Reindex

Accessing the following URL initializes/sets back the last change token. NO INDEXING IS DONE
</br>
`http://localhost:8983/solr/admin/cores?core=nemaki&action=init&repositoryId=<repoId>`

Accessing the following URL executes full reindexing AND initializes the last changeToken.
</br>
`http://localhost:8983/solr/admin/cores?core=nemaki&action=index&tracking=FULL&repositoryId=<repoId>`

Force Index via URL:
</br>
`http://localhost:8983/solr/admin/cores?core=nemaki&action=index&tracking=AUTO&repositoryId=bedroom`

action can be _index_, _init_ or _change_password_
tracking can be _AUTO_, _FULL_ or _DELTA_
-> see `NemakiCoreAdminHandler.java`
